local MENU_SELECTION_1 = 0x00FFA5DC
local MENU_SELECTION_2 = 0x00FFB77A

local TextHeight = 8

local PrimaryItemsInLevel = 0
local SecondaryItemsInLevel = 0
local DroppedPresentsInLevel = 0
local EnemiesInLevel = 0

local SLOW_INPUT_CUSION = 5
local FAST_INPUT_CUSION = 5
local slowCounter = 0
local fastCounter = 0

local displayKeyboardCommands = false
local displayLevelItems = true
local displayLevelEnemies = false
local displaySpriteFlags = false
local displaySpriteTableMemoryPane = false
local moveFast = false

local Timers = 
{
	toejamTomatoeRainTimer1 = 0xFFE1E8,
	toejamTomatoeRainTimer2 = 0xFFE1EA,
}

local GameItems =
{
	[00] = "Icarus Wings",
	[01] = "Spring Shoes",
	[02] = "Innertube",
	[03] = "Tomatoes",
	[04] = "Sling Shot",
	[05] = "Rocket Skates",
	[06] = "Rose Bushes",
	[07] = "Super Hitops",
	[08] = "Doorway",
	[09] = "Food",
	[10] = "Rootbeer",
	[11] = "Promotion",
	[12] = "Un-fall",
	[13] = "Rain Cloud",
	[14] = "Fudge Sundae",
	[15] = "Decoy",
	[16] = "Total Bummer",
	[17] = "Extra Life",
	[18] = "Randomizer",
	[19] = "Telephone",
	[20] = "Extra Buck",
	[21] = "Jackpot",
	[22] = "Tomato Rain",
	[23] = "Earthling",
	[24] = "School Book",
	[25] = "BoomBox",
	[26] = "Random",
	[27] = "Bonus Hi Tops",
	[64] = "Burger",
	[65] = "Fudge Sundae",
	[67] = "Candy Cane 1",
	[68] = "Fries",
	[69] = "Pancakes",
	[70] = "Watermelon",	
	[71] = "Bacon and Eggs",
	[72] = "Raspberry Pie",
	[73] = "Candy Cane 2",
	[76] = "Moldy Cheese",
	[77] = "Moldy Bread",
	[79] = "Cabbage",
	[80] = "A Buck",
	[81] = "Tree 1 (Regular)",
	[82] = "Tree 2 (Palm)",
	[83] = "Tree 3 (Cactus)",
	[255] = "EMPTY",
}

local EnemyTypes = 
{	
	[00] = "Ship Piece",
	[01] = "Devil",
	[02] = "Hamster",
	[03] = "Shopper",
	[04] = "Doctor",
	[05] = "Hula",
	[06] = "Bees",
	[07] = "Cupid",
	[08] = "Whirlwind",
	[09] = "Mail Order",
	[10] = "Wizard",
	[11] = "Carrotman",
	[12] = "Nerds",
	[13] = "Lawnmower",
	[14] = "Shark",
	[15] = "Chickens",
	[16] = "Boogyman",
	[17] = "Viking",
	[18] = "Mole",
	[19] = "Icecream Van",
	[20] = "Santa",
	[21] = "Doctor",
	[22] = "Bees",
	[23] = "Lemonade",
	[24] = "Hottub",
	[255] = "EMPTY",
}

local Elevator = 
{
	addr = 0xFFDA4A,
}
		
local LevelItems =
{
	primaryFirstAddress = 0xFFDAE2,
    secondaryFirstAddress = 0xFFDBE2,
	typeOffset = 0x0,
	unknown1Offset = 0x1,
	unknown1Offset = 0x2,
	unknown1Offset = 0x3,
	xPosOffset = 0x4,
	yPosOffset = 0x6,
	itemLength = 8,
	numItems = 31,
	colour = 0x00FF007F,
}

local DroppedPresents =
{ 
	firstAddress = 0xFFDCE6,
	typeOffset = 0x0,
	levelOffset = 0x1,
	unknown1Offset = 0x2,
	unknown1Offset = 0x3,
	xPosOffset = 0x4,
	yPosOffset = 0x6,
	itemLength = 8,
	numItems = 31,
	colour = 0xFF00007F,
}

local Enemies = 
{
	firstAddress = 0xFFDE72,
	typeOffset = 0x0, --Maybe 2 bytes?
	typeOffset2 = 0x1, --Maybe 2 bytes?
	unknown1Offset = 0x2,
	stateOffset = 0x3, -- 2 Asleep, also directions 
	unknown1Offset= 0x4,
	timerOffset = 0x5,
	xPosOffset = 0x6,
	yPosOffset = 0x8,
	unknown1Offset = 0xA,
	unknown1Offset = 0xB,
	unknown1Offset = 0xC,
	unknown1Offset = 0xD,
	unknown1Offset = 0xE,
	unknown1Offset = 0xF,
	currentHealthOffset = 0x10,
	unknown1Offset = 0x11, --Not Max Health
	itemLength = 18,
	numItems = 38,
    currentItem = 0,
	colour = 0x00F0FF9F,
}

local Toejam =
{
	currentLevel = 0xFF912C,
	livesAddr = 0xFFA248,
	cashAddr = 0xFFA24A,
	scoreAddr = 0xFFA24C,
	promotionLevelAddr = 0xFFA250,
	healthAddr = 0xFFA252,
	xAddr = 0xFFA25A,
	yAddr = 0xFFA25C,
	zAddr = 0xFFA25E,
	xVelocity = 0xFFA260,
	yVelocity = 0xFFA262,
	zVelocity = 0xFFA264,
	xAcceleration = 0xFFA266,
	yAcceleration = 0xFFA268,
	zAcceleration = 0xFFA26A,
	sprite = 0x00FFA2A5,
	presentTimerAddr = 0xFFDE52,
	sleepTimerAddr = 0xFFDA44,
	presentInventory = 0xFFDAC2,
}

local Earl =
{
	livesAddr = 0xFFA249,
	cashAddr = 0xFFA24B,
	scoreAddr = 0xFFA24E,
	promotionLevelAddr = 0xFFA251,
	healthAddr = 0xFFA253,
	xAddr = 0xFFA25E, --Wrong
	yAddr = 0xFFA260, --Wrong
	presentTimerAddr = 0xFFDE54, --Guess
	sleepTimerAddr = 0xFFDA46,
	--presentInvetor = 0xFFFFFF,
}

local SpriteFlags = 
{
    firstAddress = 0xFFD9C6,
    itemLength = 1,
	numItems = 26,
	colour = 0xFFFFFFFF,
}

local SpriteTable =
{
    firstAddress = 0x00FFA25A,
    xOffset = 0x00,
    yOffset = 0x02,
    zOffset = 0x04,
    xVelOffset = 0x06,
    yVelOffset = 0x08,
    zVelOffset = 0x0A,
    xAccelerationOffset = 0x0C,
    yAccelerationOffset = 0x0E,
    zAccelerationOffset = 0x10,
    animationSpriteOffset = 0x24,
    animationLoopOffset = 0x26,
    animationLoopMaxOffset = 0x27,
    animationLoopTimerOffset = 0x28,
    animationFlipOffset = 0x2C,
    spriteOffset = 0x4B,
    nextSpriteOffset = 0x5C,
    itemLength = 128,
    numItems = 40,
    currentItem = 0,
	colour = 0xFFFFFFAA,
}

local function ToHex(num)
	local asHex = ""
	while num > 0 do
		local rem = math.fmod(num, 16)
		asHex = string.sub("0123456789ABCDEF", rem+1, rem+1)..asHex
		num = (num - rem)/ 16
	end
	if asHex == "" then
		asHex = "0"
	end
	if string.len(asHex) % 2 == 1 then
		asHex = "0" .. asHex
	end
	return asHex
end

local function ToScreenCoords(x, y)
	return x-128, y-128
end

local function drawToejamTomatoeRainTimer(x, y)
	local presentTimer = memory.readbyte(Timers.toejamTomatoeRainTimer1)		
	gui.drawtext(x, y, presentTimer)
	presentTimer = memory.readbyte(Timers.toejamTomatoeRainTimer2)		
	gui.drawtext(x + 40, y, presentTimer)
end

local function drawToejamPresentTimer(x, y)
	local presentTimer = memory.readword(Toejam.presentTimerAddr)		
	gui.drawtext(x, y, presentTimer)
end

local function drawEarlPresentTimer(x, y)
	local presentTimer = memory.readword(Earl.presentTimerAddr)		
	gui.drawtext(x, y, presentTimer)
end

local function drawToejamSleepTimer(x, y)
	local sleepTimer = memory.readword(Toejam.sleepTimerAddr)		
	gui.drawtext(x, y, sleepTimer)
end

local function drawEarlSleepTimer(x, y)
	local sleepTimer = memory.readword(Earl.sleepTimerAddr)		
	gui.drawtext(x, y, sleepTimer)
end

local function drawToejamLocation(x, y)
	local p1X = memory.readword(Toejam.xAddr)			
	local p1Y = memory.readword(Toejam.yAddr)
	local p1Z = memory.readword(Toejam.zAddr)
	local p1XVelocity = memory.readword(Toejam.xVelocity)			
	local p1YVelocity = memory.readword(Toejam.yVelocity)
	local p1ZVelocity = memory.readword(Toejam.zVelocity)
	local p1XAcceleration = memory.readword(Toejam.xAcceleration)
	local p1YAcceleration = memory.readword(Toejam.yAcceleration)
	local p1ZAcceleration = memory.readword(Toejam.zAcceleration)
	gui.drawtext(x, y, "X,Y,Z: " .. ToHex(p1X) .. "," .. ToHex(p1Y) .. "," .. ToHex(p1Z))
	gui.drawtext(x, y + TextHeight, "Vel: " .. ToHex(p1XVelocity) .. "," .. ToHex(p1YVelocity) .. "," .. ToHex(p1ZVelocity))
	gui.drawtext(x, y + (TextHeight * 2), "Acc: " .. ToHex(p1XAcceleration) .. "," .. ToHex(p1YAcceleration) .. "," .. ToHex(p1ZAcceleration))
end

local function drawPlayer(player)
	local y = memory.readword(0xFFB722)
	local x = memory.readword(0xFFB728)
	x,y = ToScreenCoords(x,y)
	gui.drawbox(x, y, x+32, y+32, '#00000000', '#00ff007f')
	
	y = memory.readword(0xFFB74A)
	x = memory.readword(0xFFB750)
	x,y = ToScreenCoords(x,y)
	gui.drawbox(x, y, x+24, y+16, '#00000000', '#00ff007f')
end

local function drawToejamSprite(x, y)
	local p1Sprite = memory.readbyte(Toejam.sprite)
	gui.drawtext(x, y, ToHex(p1Sprite))
end

local function drawProgramCounter(x, y)
	local pcValue = memory.getregister("pc")
    gui.drawtext(x, y, "PC: " .. ToHex(pcValue))
end

local function drawToejamLevel(x, y)
	local p1Level = memory.readbyte(Toejam.currentLevel)			
	gui.drawtext(x, y, p1Level)
end

local function drawEarlLocation(x, y)
	local p2X = memory.readword(Earl.xAddr)			
	local p2Y = memory.readword(Earl.yAddr)
	gui.drawtext(x, y, ToHex(p2X) .. "," .. ToHex(p2Y))
end

local function drawLevelItems(x, y, itemNo, startAddress)
	local baseOffset = (itemNo*LevelItems.itemLength) + startAddress
	local itemType = memory.readbyte(baseOffset  + LevelItems.typeOffset)
	local itemName = GameItems[itemType]
	local itemXPos = memory.readword(baseOffset + LevelItems.xPosOffset)
	local itemYPos = memory.readword(baseOffset + LevelItems.yPosOffset)
	if (itemName == nil) then itemName = "UNKNOWN" end
	if (itemName ~= "EMPTY") then
		gui.drawtext(x, y, ToHex(baseOffset) .. " " .. ToHex(itemType) .. "-" .. itemName .. " (" .. ToHex(itemXPos) .. "," .. ToHex(itemYPos) .. ")", LevelItems.colour)
		if (startAddress == LevelItems.primaryFirstAddress) then PrimaryItemsInLevel = PrimaryItemsInLevel + 1 end
        if (startAddress == LevelItems.secondaryFirstAddress) then SecondaryItemsInLevel = SecondaryItemsInLevel + 1 end
	end
end

local function drawDroppedPresent(x, y, itemNo)
	local baseOffset = (itemNo*DroppedPresents.itemLength) + DroppedPresents.firstAddress
	local itemType = memory.readbyte(baseOffset  + DroppedPresents.typeOffset)
	local itemName = GameItems[itemType]
	local itemXPos = memory.readword(baseOffset + DroppedPresents.xPosOffset)
	local itemYPos = memory.readword(baseOffset + DroppedPresents.yPosOffset)
	if (itemName ~= nil and itemName ~= "EMPTY") then
		gui.drawtext(x, y, ToHex(baseOffset) .. " " .. itemName .. "(" .. ToHex(itemXPos) .. "," .. ToHex(itemYPos) .. ")", DroppedPresents.colour)
		DroppedPresentsInLevel = DroppedPresentsInLevel + 1
	end
end

local function drawEnemy(x, y, itemNo)
	local baseOffset = (itemNo*Enemies.itemLength) + Enemies.firstAddress
	local itemType = memory.readbyte(baseOffset + Enemies.typeOffset)			
	local itemName = EnemyTypes[itemType]
	local itemXPos = memory.readword(baseOffset + Enemies.xPosOffset)
	local itemYPos = memory.readword(baseOffset + Enemies.yPosOffset)
	local currentHealth = memory.readbyte(baseOffset + Enemies.currentHealthOffset)
	if (itemName ~= nil and itemName ~= "EMPTY" and itemXPos ~= 0) then	
		gui.drawtext(x, y, ToHex(baseOffset) .. " " .. itemName .. "(" .. ToHex(itemXPos) .. "," .. ToHex(itemYPos) .. ")" .. " " .. ToHex(currentHealth), Enemies.colour)
		EnemiesInLevel = EnemiesInLevel + 1
	end
end

local function drawSpriteFlag(x, y, itemNo)
    local baseOffset = itemNo + SpriteFlags.firstAddress	
    local flagValue = memory.readbyte(baseOffset)			
    gui.drawtext(x, y, ToHex(itemNo), SpriteFlags.colour)
    gui.drawtext(x, y + TextHeight + 2, ToHex(flagValue), SpriteFlags.colour)
end

local function drawLevelObjects()
    for itemNo=0,LevelItems.numItems do
        drawLevelItems(5, (PrimaryItemsInLevel * TextHeight) + TextHeight, itemNo, LevelItems.primaryFirstAddress)
        drawLevelItems(250, (SecondaryItemsInLevel * TextHeight) + TextHeight, itemNo, LevelItems.secondaryFirstAddress)
    end	
    local itemsInLevelFinishingHeight = (PrimaryItemsInLevel * TextHeight) + (TextHeight * 2)

    for itemNo=0,DroppedPresents.numItems do
        drawDroppedPresent(5, (DroppedPresentsInLevel * TextHeight) + itemsInLevelFinishingHeight, itemNo)
    end
    gui.drawtext(295, 0, "I", 0xFFFFFF7F)
end

local function drawLevelEnemies()
    for enemyNo=0,Enemies.numItems+1 do
        if (EnemiesInLevel<=19) then
            drawEnemy(0, (EnemiesInLevel * TextHeight) + TextHeight, enemyNo)
        else
            drawEnemy(220, ((EnemiesInLevel - 20) * TextHeight) + TextHeight, enemyNo)
        end
    end	
    gui.drawtext(300, 0, "E", 0xFFFFFF7F)
end

local function drawSpriteFlags()
    for flagNo=0,SpriteFlags.numItems do
        x = 5 + (flagNo * 10)
        y = 40
        
        if flagNo>=SpriteFlags.numItems/2 then
            x = x - ((SpriteFlags.numItems / 2) * 10)
            y = y + 20
        end
        drawSpriteFlag(x, y, flagNo)
    end
end

local function drawKeyboardCommands()
    gui.drawbox (0, 45, 125, 150, 0x000055FF, 0xFFFFFFFF)
    gui.drawtext(5, 50, "E - (E)nemies in level", 0xFFFFFFFF)
    gui.drawtext(5, 58, "F - (F)ast mode", 0xFFFFFFFF)
    gui.drawtext(5, 66, "G - Sprite Fla(G)s", 0xFFFFFFFF)
    gui.drawtext(5, 74, "I - (I)tems in level", 0xFFFFFFFF)
    gui.drawtext(5, 82, "K - (K)eyboard Commands", 0xFFFFFFFF)
    gui.drawtext(5, 90, "L - Teleport to e(L)evator", 0xFFFFFFFF)
    gui.drawtext(5, 98, "M - (M)emory Pane", 0xFFFFFFFF)
    gui.drawtext(5, 106, "O - Decrease present type", 0xFFFFFFFF)
    gui.drawtext(5, 114, "P - Increase present type", 0xFFFFFFFF)
end

local function drawLevelEnemiesMemoryPane(x, y, startAddress, length)
    startAddress = startAddress + (Enemies.currentItem * length)
    height = TextHeight * length
    width = 150
    startX = x
    startY = y
    
    --Background, heading and keyboard controls
    gui.drawbox (x, y, x + width, y + height, 0x000055AA, Enemies.colour)
    gui.drawtext(x + 5, y + 2, "<< PgDn  Enemy " .. Enemies.currentItem .. " - " .. ToHex(startAddress) .. "  PgUp >>", Enemies.colour)

    --For each of the addresses
    unknownOffset = ""
    unknownData = ""
    readSize = ""
    addressIndex = 0
    while(addressIndex < length) do
        --If we are halfway through, start a new column
        if (addressIndex == (length / 2)-12) then
            x = startX + (width/2)
            y = startY
        end
        
        if (addressIndex == Enemies.typeOffset) then
            if (unknownOffset ~= "") then
                gui.drawtext(x + 5, y + 12, unknownOffset .. ": " .. unknownData, Enemies.colour)
                unknownOffset = ""
                unknownData = ""
                y = y + TextHeight
            end
            gui.drawtext(x + 5, y + 12, "Type" .. ": " .. ToHex(memory.readword(startAddress + Enemies.typeOffset)) .. " - " .. EnemyTypes[memory.readbyte(startAddress + Enemies.typeOffset)], Enemies.colour)
            readSize = "word"
            y = y + TextHeight
        elseif (addressIndex == Enemies.stateOffset) then
            if (unknownOffset ~= "") then
                gui.drawtext(x + 5, y + 12, unknownOffset .. ": " .. unknownData, Enemies.colour)
                unknownOffset = ""
                unknownData = ""
                y = y + TextHeight
            end
            gui.drawtext(x + 5, y + 12, "State" .. ": " .. ToHex(memory.readbyte(startAddress + Enemies.stateOffset)), Enemies.colour)
            readSize = "byte"
            y = y + TextHeight
        elseif (addressIndex == Enemies.timerOffset) then
            if (unknownOffset ~= "") then
                gui.drawtext(x + 5, y + 12, unknownOffset .. ": " .. unknownData, Enemies.colour)
                unknownOffset = ""
                unknownData = ""
                y = y + TextHeight
            end
            gui.drawtext(x + 5, y + 12, "Timer" .. ": " .. ToHex(memory.readbyte(startAddress + Enemies.timerOffset)), Enemies.colour)
            readSize = "byte"
            y = y + TextHeight
        elseif (addressIndex == Enemies.xPosOffset) then
            if (unknownOffset ~= "") then
                gui.drawtext(x + 5, y + 12, unknownOffset .. ": " .. unknownData, Enemies.colour)
                unknownOffset = ""
                unknownData = ""
                y = y + TextHeight
            end
            gui.drawtext(x + 5, y + 12, "xPos" .. ": " .. ToHex(memory.readword(startAddress + Enemies.xPosOffset)), Enemies.colour)
            readSize = "word"
            y = y + TextHeight
        elseif (addressIndex == Enemies.yPosOffset) then
            if (unknownOffset ~= "") then
                gui.drawtext(x + 5, y + 12, unknownOffset .. ": " .. unknownData, Enemies.colour)
                unknownOffset = ""
                unknownData = ""
                y = y + TextHeight
            end
            gui.drawtext(x + 5, y + 12, "yPos" .. ": " .. ToHex(memory.readword(startAddress + Enemies.yPosOffset)), Enemies.colour)
            readSize = "word"
            y = y + TextHeight
        elseif (addressIndex == Enemies.currentHealthOffset) then
            if (unknownOffset ~= "") then
                gui.drawtext(x + 5, y + 12, unknownOffset .. ": " .. unknownData, Enemies.colour)
                unknownOffset = ""
                unknownData = ""
                y = y + TextHeight
            end
            gui.drawtext(x + 5, y + 12, "Health" .. ": " .. ToHex(memory.readbyte(startAddress + Enemies.currentHealthOffset)), Enemies.colour)
            readSize = "byte"
            y = y + TextHeight
        else
            if (unknownOffset == "") then
                unknownOffset = ToHex(startAddress + addressIndex)
            end
            unknownData = unknownData .. ToHex(memory.readbyte(startAddress + addressIndex))
            readSize = "byte"
            if (string.len(unknownData) == 8) then
                gui.drawtext(x + 5, y + 12, unknownOffset .. ": " .. unknownData, Enemies.colour)
                unknownOffset = ""
                unknownData = ""
                y = y + TextHeight
            end
        end

        if (readSize == "byte") then
            addressIndex = addressIndex + 1
        elseif (readSize == "word") then
            addressIndex = addressIndex + 2
        elseif (readSize == "long") then
            addressIndex = addressIndex + 4
        end
   end
   if (unknownOffset ~= "") then
        gui.drawtext(x + 5, y + 12, unknownOffset .. ": " .. unknownData, Enemies.colour)
   end
end

local function drawSpriteTableMemoryPane(x, y, startAddress, length)
    --Currently hard coded for the SpriteTable data
    startAddress = startAddress + (SpriteTable.currentItem * length)
    height = TextHeight * (length/5)
    width = 150
    startX = x
    startY = y
    
    --Background, heading and keyboard controls
    gui.drawbox (x, y, x + width, y + height, 0x000055AA, SpriteTable.colour)
    gui.drawtext(x + 5, y + 2, "<< PgDn  Sprite " .. SpriteTable.currentItem .. " - " .. ToHex(startAddress) .. "  PgUp >>", SpriteTable.colour)

    --For each of the addresses
    unknownOffset = ""
    unknownData = ""
    readSize = ""
    addressIndex = 0
    while(addressIndex < length ) do
        --If we are halfway through, start a new column
        if (addressIndex == (length / 2)-8) then
            x = startX + (width/2)
            y = startY
        end
        
        if (addressIndex == SpriteTable.xOffset) then
            if (unknownOffset ~= "") then
                gui.drawtext(x + 5, y + 12, unknownOffset .. ": " .. unknownData, SpriteTable.colour)
                unknownOffset = ""
                unknownData = ""
                y = y + TextHeight
            end
            gui.drawtext(x + 5, y + 12, "xPos" .. ": " .. ToHex(memory.readword(startAddress + SpriteTable.xOffset)), SpriteTable.colour)
            readSize = "word"
            y = y + TextHeight
        elseif (addressIndex == SpriteTable.yOffset) then
            if (unknownOffset ~= "") then
                gui.drawtext(x + 5, y + 12, unknownOffset .. ": " .. unknownData, SpriteTable.colour)
                unknownOffset = ""
                unknownData = ""
                y = y + TextHeight
            end
            gui.drawtext(x + 5, y + 12, "yPos" .. ": " .. ToHex(memory.readword(startAddress + SpriteTable.yOffset)), SpriteTable.colour)
            readSize = "word"
            y = y + TextHeight
        elseif (addressIndex == SpriteTable.zOffset) then
            if (unknownOffset ~= "") then
                gui.drawtext(x + 5, y + 12, unknownOffset .. ": " .. unknownData, SpriteTable.colour)
                unknownOffset = ""
                unknownData = ""
                y = y + TextHeight
            end
            gui.drawtext(x + 5, y + 12, "zPos" .. ": " .. ToHex(memory.readword(startAddress + SpriteTable.zOffset)), SpriteTable.colour)
            readSize = "word"
            y = y + TextHeight
        elseif (addressIndex == SpriteTable.xVelOffset) then
            if (unknownOffset ~= "") then
                gui.drawtext(x + 5, y + 12, unknownOffset .. ": " .. unknownData, SpriteTable.colour)
                unknownOffset = ""
                unknownData = ""
                y = y + TextHeight
            end
            gui.drawtext(x + 5, y + 12, "xVel" .. ": " .. ToHex(memory.readword(startAddress + SpriteTable.xVelOffset)), SpriteTable.colour)
            readSize = "word"
            y = y + TextHeight
        elseif (addressIndex == SpriteTable.yVelOffset) then
            if (unknownOffset ~= "") then
                gui.drawtext(x + 5, y + 12, unknownOffset .. ": " .. unknownData, SpriteTable.colour)
                unknownOffset = ""
                unknownData = ""
                y = y + TextHeight
            end
            gui.drawtext(x + 5, y + 12, "yVel" .. ": " .. ToHex(memory.readword(startAddress + SpriteTable.yVelOffset)), SpriteTable.colour)
            readSize = "word"
            y = y + TextHeight
        elseif (addressIndex == SpriteTable.zVelOffset) then
            if (unknownOffset ~= "") then
                gui.drawtext(x + 5, y + 12, unknownOffset .. ": " .. unknownData, SpriteTable.colour)
                unknownOffset = ""
                unknownData = ""
                y = y + TextHeight
            end
            gui.drawtext(x + 5, y + 12, "zVel" .. ": " .. ToHex(memory.readword(startAddress + SpriteTable.zVelOffset)), SpriteTable.colour)
            readSize = "word"
            y = y + TextHeight
        elseif (addressIndex == SpriteTable.xAccelerationOffset) then
            if (unknownOffset ~= "") then
                gui.drawtext(x + 5, y + 12, unknownOffset .. ": " .. unknownData, SpriteTable.colour)
                unknownOffset = ""
                unknownData = ""
                y = y + TextHeight
            end
            gui.drawtext(x + 5, y + 12, "xAcc" .. ": " .. ToHex(memory.readword(startAddress + SpriteTable.xAccelerationOffset)), SpriteTable.colour)
            readSize = "word"
            y = y + TextHeight
        elseif (addressIndex == SpriteTable.yAccelerationOffset) then
            if (unknownOffset ~= "") then
                gui.drawtext(x + 5, y + 12, unknownOffset .. ": " .. unknownData, SpriteTable.colour)
                unknownOffset = ""
                unknownData = ""
                y = y + TextHeight
            end
            gui.drawtext(x + 5, y + 12, "yAcc" .. ": " .. ToHex(memory.readword(startAddress + SpriteTable.yAccelerationOffset)), SpriteTable.colour)
            readSize = "word"
            y = y + TextHeight
        elseif (addressIndex == SpriteTable.zAccelerationOffset) then
            if (unknownOffset ~= "") then
                gui.drawtext(x + 5, y + 12, unknownOffset .. ": " .. unknownData, SpriteTable.colour)
                unknownOffset = ""
                unknownData = ""
                y = y + TextHeight
            end
            gui.drawtext(x + 5, y + 12, "zAcc" .. ": " .. ToHex(memory.readword(startAddress + SpriteTable.zAccelerationOffset)), SpriteTable.colour)
            readSize = "word"
            y = y + TextHeight
        elseif (addressIndex == SpriteTable.animationSpriteOffset) then
            if (unknownOffset ~= "") then
                gui.drawtext(x + 5, y + 12, unknownOffset .. ": " .. unknownData, SpriteTable.colour)
                unknownOffset = ""
                unknownData = ""
                y = y + TextHeight
            end
            gui.drawtext(x + 5, y + 12, "aSprite" .. ": " .. ToHex(memory.readword(startAddress + SpriteTable.animationSpriteOffset)), SpriteTable.colour)
            readSize = "word"
            y = y + TextHeight
        elseif (addressIndex == SpriteTable.animationLoopOffset) then
            if (unknownOffset ~= "") then
                gui.drawtext(x + 5, y + 12, unknownOffset .. ": " .. unknownData, SpriteTable.colour)
                unknownOffset = ""
                unknownData = ""
                y = y + TextHeight
            end
            gui.drawtext(x + 5, y + 12, "aLoop" .. ": " .. ToHex(memory.readbyte(startAddress + SpriteTable.animationLoopOffset)), SpriteTable.colour)
            readSize = "byte"
            y = y + TextHeight
        elseif (addressIndex == SpriteTable.animationLoopMaxOffset) then
            if (unknownOffset ~= "") then
                gui.drawtext(x + 5, y + 12, unknownOffset .. ": " .. unknownData, SpriteTable.colour)
                unknownOffset = ""
                unknownData = ""
                y = y + TextHeight
            end
            gui.drawtext(x + 5, y + 12, "aMax" .. ": " .. ToHex(memory.readbyte(startAddress + SpriteTable.animationLoopMaxOffset)), SpriteTable.colour)
            readSize = "byte"
            y = y + TextHeight
        elseif (addressIndex == SpriteTable.animationLoopTimerOffset) then
            if (unknownOffset ~= "") then
                gui.drawtext(x + 5, y + 12, unknownOffset .. ": " .. unknownData, SpriteTable.colour)
                unknownOffset = ""
                unknownData = ""
                y = y + TextHeight
            end
            gui.drawtext(x + 5, y + 12, "aTime" .. ": " .. ToHex(memory.readbyte(startAddress + SpriteTable.animationLoopTimerOffset)), SpriteTable.colour)
            readSize = "byte"
            y = y + TextHeight
        elseif (addressIndex == SpriteTable.animationFlipOffset) then
            if (unknownOffset ~= "") then
                gui.drawtext(x + 5, y + 12, unknownOffset .. ": " .. unknownData, SpriteTable.colour)
                unknownOffset = ""
                unknownData = ""
                y = y + TextHeight
            end
            gui.drawtext(x + 5, y + 12, "aFlip" .. ": " .. ToHex(memory.readbyte(startAddress + SpriteTable.animationFlipOffset)), SpriteTable.colour)
            readSize = "byte"
            y = y + TextHeight
        elseif (addressIndex == SpriteTable.nextSpriteOffset) then
            if (unknownOffset ~= "") then
                gui.drawtext(x + 5, y + 12, unknownOffset .. ": " .. unknownData, SpriteTable.colour)
                unknownOffset = ""
                unknownData = ""
                y = y + TextHeight
            end
            gui.drawtext(x + 5, y + 12, "Next" .. ":   " .. ToHex(memory.readword(startAddress + addressIndex)), SpriteTable.colour)
            readSize = "word"
            y = y + TextHeight
        elseif (addressIndex == SpriteTable.nextSpriteOffset + 2) then
            if (unknownOffset ~= "") then
                gui.drawtext(x + 5, y + 12, unknownOffset .. ": " .. unknownData, SpriteTable.colour)
                unknownOffset = ""
                unknownData = ""
                y = y + TextHeight
            end
            gui.drawtext(x + 5, y + 12, "P.Oset" .. ": " .. ToHex(memory.readword(startAddress + addressIndex)), SpriteTable.colour)
            readSize = "word"
            y = y + TextHeight
        elseif (addressIndex == SpriteTable.spriteOffset) then
            if (unknownOffset ~= "") then
                gui.drawtext(x + 5, y + 12, unknownOffset .. ": " .. unknownData, SpriteTable.colour)
                unknownOffset = ""
                unknownData = ""
                y = y + TextHeight
            end
            gui.drawtext(x + 5, y + 12, "Sprite" .. ": " .. ToHex(memory.readbyte(startAddress + SpriteTable.spriteOffset)), SpriteTable.colour)
            readSize = "byte"
            y = y + TextHeight
        else
            if (unknownOffset == "") then
                unknownOffset = ToHex(startAddress + addressIndex)
            end
            unknownData = unknownData .. ToHex(memory.readbyte(startAddress + addressIndex))
            readSize = "byte"
            if (string.len(unknownData) == 8) then
                gui.drawtext(x + 5, y + 12, unknownOffset .. ": " .. unknownData, SpriteTable.colour)
                unknownOffset = ""
                unknownData = ""
                y = y + TextHeight
            end
        end

        if (readSize == "byte") then
            addressIndex = addressIndex + 1
        elseif (readSize == "word") then
            addressIndex = addressIndex + 2
        elseif (readSize == "long") then
            addressIndex = addressIndex + 4
        end
    end
    if (unknownOffset ~= "") then
        gui.drawtext(x + 5, y + 12, unknownOffset .. ": " .. unknownData, Enemies.colour)
    end
end

local function teleportToejamToElevator()
	newLocX = memory.readword(Elevator.addr)
	newLocY = memory.readword(Elevator.addr+2)
	memory.writeword(Toejam.xAddr, newLocX)
	memory.writeword(Toejam.yAddr, newLocY+10)
	--Move camera DOESN'T WORK
	memory.writeword(0xFF8002, newLocX-0xA1)
	memory.writeword(0xFF8036, newLocX-0xA1)
	memory.writeword(0xFF8004, newLocY-0x7F)
	memory.writeword(0xFF8038, newLocY-0x7F)
end

local function addToMemory(address, amount)
	newValue = memory.readbyte(address)+amount	
	memory.writebyte(address, newValue)
	name = GameItems[newValue]
	if name == nil then name = "Unknown" end
	print(ToHex(newValue) .. "-" .. name)
end

local function setMemory(address, value)
	memory.writebyte(address, value)
end

local function resetToejamSleepTimer()
	setMemory(Toejam.sleepTimerAddr+1, 0x0000)
end

local function breakPointHit(address, size)
    print("Breakpoint hit: " .. ToHex(address) .. " - " .. ToHex(memory.readbyte(address)) .. " PC: " .. ToHex(memory.getregister("pc")))
    gens.pause()
end

local function readAndActOnInput()
	buttons = joypad.get(1)
	if moveFast then
		if buttons.up then
			newLocY = memory.readword(Toejam.yAddr)
			memory.writeword(Toejam.yAddr, newLocY-10)
		end
		
		if buttons.down then
			newLocY = memory.readword(Toejam.yAddr)
			memory.writeword(Toejam.yAddr, newLocY+10)
		end
		
		if buttons.left then
			newLocX = memory.readword(Toejam.xAddr)
			memory.writeword(Toejam.xAddr, newLocX-10)
		end
		
		if buttons.right then
			newLocX = memory.readword(Toejam.xAddr)
			memory.writeword(Toejam.xAddr, newLocX+10)
		end	
		gui.drawtext(305, 0, "F", 0xFFFFFF7F)
	end
	
	mouseAndKeys = input.get()
	if mouseAndKeys.leftclick then
		--addToMemory(0xFFDAE2, 1)
	end 
	
	if mouseAndKeys.rightclick then
		--addToMemory(0xFFDAE2, -1)
	end 

	if mouseAndKeys.E then 
		if slowCounter < SLOW_INPUT_CUSION then
			slowCounter = slowCounter + 1
		else
			displayLevelEnemies = not displayLevelEnemies
			slowCounter = 0
		end				
	end
	
	if mouseAndKeys.F then 
		if slowCounter < SLOW_INPUT_CUSION then
			slowCounter = slowCounter + 1
		else
			moveFast = not moveFast
			slowCounter = 0
		end				
	end

	if mouseAndKeys.G then 
		if slowCounter < SLOW_INPUT_CUSION then
			slowCounter = slowCounter + 1
		else
			displaySpriteFlags = not displaySpriteFlags
			slowCounter = 0
		end				
	end

	if mouseAndKeys.I then 
		if slowCounter < SLOW_INPUT_CUSION then
			slowCounter = slowCounter + 1
		else
			displayLevelItems = not displayLevelItems
			slowCounter = 0
		end				
	end
    
    if mouseAndKeys.K then 
		if slowCounter < SLOW_INPUT_CUSION then
			slowCounter = slowCounter + 1
		else
			displayKeyboardCommands = not displayKeyboardCommands
			slowCounter = 0
		end				
	end

    if mouseAndKeys.L then 
		if slowCounter < SLOW_INPUT_CUSION then
			slowCounter = slowCounter + 1
		else
			teleportToejamToElevator()
			slowCounter = 0
		end				
    end
    
    if mouseAndKeys.M then 
		if slowCounter < SLOW_INPUT_CUSION then
			slowCounter = slowCounter + 1
		else
			displaySpriteTableMemoryPane = not displaySpriteTableMemoryPane
			slowCounter = 0
		end				
	end

	if mouseAndKeys.O then 
		if slowCounter < SLOW_INPUT_CUSION then
			slowCounter = slowCounter + 1
		else
			addToMemory(Toejam.presentInventory, -1)
			slowCounter = 0
		end				
	end

	if mouseAndKeys.P then 
		if slowCounter < SLOW_INPUT_CUSION then
			slowCounter = slowCounter + 1
		else
			addToMemory(Toejam.presentInventory, 1)
			slowCounter = 0
		end				
    end
    
    if mouseAndKeys.pageup then
		if slowCounter < SLOW_INPUT_CUSION then
			slowCounter = slowCounter + 1
		elseif (displaySpriteTableMemoryPane) then
            SpriteTable.currentItem = SpriteTable.currentItem + 1
            if (SpriteTable.currentItem == SpriteTable.numItems) then
                SpriteTable.currentItem = 0
            end 
            slowCounter = 0		
        elseif (displayLevelEnemies) then
            Enemies.currentItem = Enemies.currentItem + 1
            if (Enemies.currentItem == Enemies.numItems) then
                Enemies.currentItem = 0
            end 
            slowCounter = 0		
		end				
    end
    
    if mouseAndKeys.pagedown then
		if slowCounter < SLOW_INPUT_CUSION then
			slowCounter = slowCounter + 1
		elseif (displaySpriteTableMemoryPane) then
            SpriteTable.currentItem = SpriteTable.currentItem - 1
            if (SpriteTable.currentItem == -1) then
                SpriteTable.currentItem = SpriteTable.numItems - 1
            end 
			slowCounter = 0
        elseif (displayLevelEnemies) then
            Enemies.currentItem = Enemies.currentItem - 1
            if (Enemies.currentItem == -1) then
                Enemies.currentItem = Enemies.numItems - 1
            end 
            slowCounter = 0	
		end				
    end

end

gens.registerbefore(function()

    resetToejamSleepTimer()
    if (not displayKeyboardCommands) then	
	    drawToejamLocation(5, 0)
		drawPlayer(Toejam)
	    --drawProgramCounter(150, 0)
	    drawToejamLevel(150, 182)
	    --drawEarlLocation(50, 0)
	    drawToejamSprite(150, 100)

	    drawToejamTomatoeRainTimer(10, 174)
        --drawEarlTomatoeRainTimer(10, 190)
        
	    drawToejamSleepTimer(10, 182)
	    drawEarlSleepTimer(190, 182)
        
	    drawToejamPresentTimer(10, 190)
	    drawEarlPresentTimer(190, 190)

	    if (displayLevelItems) then
            drawLevelObjects()
	    end

	    if (displayLevelEnemies) then
	    	drawLevelEnemies()
            drawLevelEnemiesMemoryPane(160, 10, Enemies.firstAddress, Enemies.itemLength)
        end

        if (displaySpriteFlags) then
	    	drawSpriteFlags()
        end

        if (displaySpriteTableMemoryPane) then
            drawSpriteTableMemoryPane(160, 10, SpriteTable.firstAddress, SpriteTable.itemLength)
        end
    else
        drawKeyboardCommands()
    end
	
	readAndActOnInput()

	PrimaryItemsInLevel = 0
    SecondaryItemsInLevel = 0
	DroppedPresentsInLevel = 0
	EnemiesInLevel = 0

end)

--Lua based breakpoints. Not as accurate as built in debugger, but saveable
--memory.registerread(0xFFDAC2, 1, breakPointHit)
--memory.registerwrite(0xFFDAC2, 1, breakPointHit)

--Place any 'on load' code below:

    print("Script loaded. Press K to display (K)ey commands.")