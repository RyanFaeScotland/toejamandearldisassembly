@ECHO OFF
REM Create build directory
IF NOT EXIST "build\" (mkdir "build")

REM Assemble source
cd "build"
IF EXIST "ToejamAndEarl.bin" (DEL "ToejamAndEarl.bin")
D:\VASM\vasmm68k_mot_win32.exe -spaces -x -chklabels -nocase -maxerrors=2000 -m68000 -L "Listing.txt" -no-opt -Fbin -o "ToejamAndEarl.bin" "..\Source\MASTER.X68"
ECHO.

REM Confirm generated binary matches original
IF EXIST "ToejamAndEarl.bin" (
    fc /b "ToejamAndEarl.bin" "..\..\Toejam & Earl (REV 00) (U) [!].bin"
) ELSE (
    ECHO ToejamAndEarl.bin does not exist, compare failed
)

REM Create EntireSource.X68 and check that it builds into the same binary
IF EXIST "ToejamAndEarl.bin" (
    cd "..\FileRebuilder\"
    IF EXIST "EntireSource.X68" (DEL "EntireSource.X68")
    CALL FileRebuilder.exe
    IF EXIST "EntireSource.X68" (
        move /Y "EntireSource.X68" "..\Source\EntireSource.X68"
        IF EXIST "ToejamAndEarl_EntireSource.bin" (DEL "ToejamAndEarl_EntireSource.bin")
        D:\VASM\vasmm68k_mot_win32.exe -spaces -x -chklabels -nocase -maxerrors=2000 -m68000 -L "Listing.txt" -no-opt -Fbin -o "ToejamAndEarl_EntireSource.bin" "..\Source\EntireSource.X68"
        IF EXIST "ToejamAndEarl_EntireSource.bin" (
            fc /b "ToejamAndEarl_EntireSource.bin" "..\..\Toejam & Earl (REV 00) (U) [!].bin"
        ) ELSE (
            ECHO Failed to build from EntireSource.X68
        )
    ) ELSE (
        ECHO Failed to generate EntireSource.X68
    )
)