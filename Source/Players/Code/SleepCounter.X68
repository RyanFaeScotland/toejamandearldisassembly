 ;
 ; The player's sleep counter. Adds 1 and checks if it is less than 300, branches if it is, starts the sleep routine if it isn't.
 ;
    ORG $00012322
ProcessSleep:
	LINK	A6, #-($04)
	MOVEM.l	A5/A4/A3/A2/D6/D5/D4/D3/D2, -(A7)
	MOVEA.l	$8(A6), A2
	MOVEQ	#-1, D0
	MOVE.w	D0, -$2(A6)
	MOVEA.l	#LOC_00FFDA40, A3
	MOVEA.l	#LOC_00FFDA42, A4
	MOVE.l	#TOEJAMS_SLEEP_TIMER, D6
	MOVE.b	$5E(A2), D5
	EXT.w	D5
	MOVE.w	D5, D0
	EXT.l	D0
	MOVEA.l	#ELEVATOR_FLAG, A0
	TST.b	(A0,D0.l)
	BNE.b	LOC_00012364
	CMPI.b	#$1A, $4C(A2)
	BNE.b	LOC_00012368
LOC_00012364:
	BRA.w	LOC_0001265A
LOC_00012368:
	CMPI.b	#$74, $4B(A2)
	BNE.b	LOC_000123B4
	MOVE.w	D5, D0
	EXT.l	D0
	MOVEA.l	#LOC_00FFDA3E, A0
	SUBQ.b	#1, (A0,D0.l)
	BGT.w	LOC_00012640
	MOVEQ	#$00000075, D4
	MOVE.w	D4, -$2(A6)
	MOVE.w	D5, D3
	EXT.l	D3
	JSR		GenerateRandom.l
	MOVE.w	D0, D2
	MOVEQ	#7, D1
	MOVEQ	#0, D0
	MOVE.w	D2, D0
	DIVU.w	D1, D0
	BVC.b	LOC_000123A6
	JSR		RandomRelated
	BRA.b	SendPlayerToSleep
LOC_000123A6:
	CLR.w	D0
	SWAP	D0
SendPlayerToSleep:
	ADDQ.l	#4, D0
	MOVE.b	D0, (A3,D3.l)
	BRA.w	LOC_00012640
LOC_000123B4:
	CMPI.b	#$75, $4B(A2)
	BNE.w	LOC_000125C8
	MOVE.w	D5, D0
	EXT.l	D0
	MOVE.l	D0, -(A7)
	JSR		MovePadInputIntoD0.l
	ADDQ.l	#4, A7
	MOVE.b	D0, D4
	MOVE.w	D5, D0
	EXT.l	D0
	MOVE.b	(A4,D0.l), D1
	EXT.w	D1
	BEQ.b	LOC_0001243C
	MOVE.w	D1, D0
	ASL.w	#7, D0
	MOVEA.l	#TOEJAMS_XPOS, A0
	ADDA.w	D0, A0
	MOVEA.l	A0, A1
	MOVE.w	D5, D0
	EXT.l	D0
	TST.b	(A3,D0.l)
	BNE.b	LOC_00012400
	MOVE.w	D5, D0
	EXT.l	D0
	ADD.l	D0, D0
	MOVEA.l	D6, A5
	SUBQ.w	#1, (A5,D0.l)
	BLE.b	LOC_00012408
LOC_00012400:
	TST.b	$28(A1)
	BNE.w	LOC_000125B6
LOC_00012408:
	MOVE.w	D1, D0
	EXT.l	D0
	MOVE.l	D0, -(A7)
	JSR		LOC_0000D950
	MOVE.w	D5, D0
	EXT.l	D0
	ADDQ.l	#4, A7
	CLR.b	(A4,D0.l)
	MOVE.w	D5, D0
	EXT.l	D0
	TST.b	(A3,D0.l)
	BNE.w	LOC_000125B6
	MOVE.l	A2, -(A7)
	JSR		GetActivePresentSprite
	ADDQ.l	#4, A7
	MOVE.w	D0, -$2(A6)
	BRA.w	LOC_000125B6
LOC_0001243C:
	TST.b	D4
	BEQ.w	LOC_00012598
	MOVE.w	D5, D0
	EXT.l	D0
	MOVEA.l	#LOC_00FFDA48, A0
	CMP.b	(A0,D0.l), D4
	BEQ.w	LOC_00012598
	CMPI.w	#$003A, LOC_00FF91E0
	BNE.b	LOC_0001246C
	CLR.w	D1
	MOVE.w	D1, LOC_00FF91E0
	MOVEQ	#0, D0
	MOVE.w	D1, D0
	BRA.b	LOC_00012480
LOC_0001246C:
	MOVEA.l	#LOC_00FF91E0, A0
	ADDQ.w	#1, (A0)
	MOVEQ	#0, D0
	MOVE.w	(A0), D0
	MOVE.w	D0, D0
	MOVEQ	#0, D2
	MOVE.w	D0, D2
	MOVE.l	D2, D0
LOC_00012480:
	MOVE.l	D0, D2
	ADD.l	D2, D2
	MOVEA.l	#PREGEN_RANDOMS, A0
	MOVEQ	#$00000064, D1
	MOVEQ	#0, D3
	MOVE.w	(A0,D2.l), D3
	MOVE.l	D3, D0
	DIVU.w	D1, D0
	BVC.b	LOC_000124A0
	JSR		RandomRelated
	BRA.b	LOC_000124A4
LOC_000124A0:
	CLR.w	D0
	SWAP	D0
LOC_000124A4:
	MOVE.l	D0, D1
	MOVEQ	#$00000014, D0
	CMP.l	D1, D0
	BLE.w	LOC_00012598
	JSR		LOC_0000D8E4
	MOVE.w	D0, D1
	BEQ.w	LOC_000125B6
	MOVE.w	D5, D0
	EXT.l	D0
	MOVE.b	D1, (A4,D0.l)
	MOVE.w	D1, D0
	ASL.w	#7, D0
	MOVEA.l	#TOEJAMS_XPOS, A0
	ADDA.w	D0, A0
	MOVEA.l	A0, A1
	MOVE.w	D5, D0
	EXT.l	D0
	SUBQ.b	#1, (A3,D0.l)
	MOVE.w	(A2), (A1)
	MOVE.w	$2(A2), $2(A1)
	MOVE.b	$4C(A2), $4C(A1)
	MOVE.b	#$40, $29(A1)
	MOVE.b	#$20, $2A(A1)
	MOVE.b	#2, $2E(A1)
	MOVE.b	#$20, $2B(A1)
	MOVE.w	#$8000, $2C(A1)
	MOVE.w	D5, D0
	EXT.l	D0
	ADD.l	D0, D0
	MOVE.l	D0, D2
	ASL.l	#2, D0
	ADD.l	D2, D0
	MOVEA.l	#$000AF048, A0
	ADDA.l	D0, A0
	MOVEA.l	A0, A4
	MOVE.l	$4(A4), $1E(A1)
	MOVE.b	#1, $28(A1)
	MOVE.b	#$FF, $26(A1)
	MOVE.w	D5, D0
	EXT.l	D0
	TST.b	(A3,D0.l)
	BEQ.b	LOC_00012568
	MOVE.b	#$0F, $27(A1)
	MOVE.w	D5, D2
	EXT.l	D2
	BTST.b	#0, (A3,D2.l)
	BEQ.b	LOC_00012552
	MOVE.l	#$000AF05C, $1A(A1)
	BRA.b	LOC_0001255A
LOC_00012552:
	MOVE.l	#$000AF05E, $1A(A1)
LOC_0001255A:
	CLR.l	-(A7)
	PEA		$0032.W
	MOVE.l	#$00087FF0, -(A7)
	BRA.b	LOC_000125AC
LOC_00012568:
	MOVE.l	(A4), $1A(A1)
	MOVE.b	$8(A4), $27(A1)
	CLR.l	-(A7)
	PEA		UNUSEDRESERVED11.W
	MOVE.l	#$0007E84A, -(A7)
	JSR		LOC_0003F56C
	MOVE.w	D5, D0
	EXT.l	D0
	ADD.l	D0, D0
	MOVEA.l	D6, A3
	LEA	$C(A7), A7
	MOVE.w	#$001E, (A3,D0.l)
	BRA.b	LOC_000125B6
LOC_00012598:
	TST.w	TOEJAM_SPEECH_COUNTER
	BNE.b	LOC_000125B6
	CLR.l	-(A7)
	PEA		($1E).W
	MOVE.l	#$000837AE, -(A7)
LOC_000125AC:
	JSR		LOC_0003F56C
	LEA		$C(A7), A7
LOC_000125B6:
	MOVE.w	D5, D0
	EXT.l	D0
	MOVEA.l	#LOC_00FFDA48, A0
	MOVE.b	D4, (A0,D0.l)
	BRA.w	LOC_00012640
LOC_000125C8:
	MOVE.w	D5, D0
	EXT.l	D0
	MOVE.b	(A4,D0.l), D1
	EXT.w	D1
	BEQ.b	SleepCounter
	MOVE.w	D1, D0
	EXT.l	D0
	MOVE.l	D0, -(A7)
	JSR		LOC_0000D950
	MOVE.w	D5, D0
	EXT.l	D0
	ADDQ.l	#4, A7
	CLR.b	(A4,D0.l)
SleepCounter: 
	TST.b	$4B(A2)
	BNE.b	LOC_00012634
	MOVE.w	D5, D2
	EXT.l	D2
	ADD.l	D2, D2					;Very likely the player offset
	MOVE.L	D6, A1					;Sleep counter address into A1
	MOVE.L	A1, A0
	;NOP
    ;NOP
    ADDQ.W	#$1, (A0,D2.L)  		;Adds one to the sleep timer
	MOVE.W	(A0,D2.L), D0			;and check the result
	CMPI.W	#TIME_BEFORE_SLEEP, D0	;if it is less than 300
	BLE.b	LOC_00012640			;branch over the sleep routine, otherwise continue on into it
	MOVEQ	#$75, D4				;Move $75 (117) into D4
	MOVE.W	D4, -$0002(A6)			;Moves the $0075 into 7F66 
	MOVE.W	D5, D3					;Player offset into D3
	EXT.L	D3
	JSR		GenerateRandom.l		;Get a random number to see how long we'll put the player to sleep for
	MOVE.W	D0, D2
	MOVEQ	#$07, D1				;Set D1 to 7
	MOVEQ	#$00, D0				;Set D0 to 0 
	MOVE.W	D2, D0					;Set D0 to our random
	DIVU	D1, D0		  			;Divide D0 (our random number) by D1 ($07)
	BVC.b	LOC_0001262C
	JSR		RandomRelated   			;Never seen		
	BRA.b	LOC_00012630	   		;Never seen
LOC_0001262C:
	CLR.W	D0			  			;Clear lower word of D0
	SWAP	D0			 			;Swap the high and low end byte, to set the wakeup counter (Gets 4 added to it later)
LOC_00012630:
	BRA		SendPlayerToSleep		;Night night time!
LOC_00012634:
	MOVE.W	D5, D0
	EXT.L	D0
	ADD.L	D0, D0
	MOVE.L	D6, A1
	CLR.W	(A1, D0.L)
LOC_00012640:
	CMPI.w	#$FFFF, -$2(A6)
	BEQ.b	LOC_0001265A
	MOVE.w	-$2(A6), D0
	EXT.l	D0
	MOVE.l	D0, -(A7)
	MOVE.l	A2, -(A7)
	JSR		SetSprite
	ADDQ.l	#8, A7
LOC_0001265A:
	MOVEM.l	-$28(A6), D2/D3/D4/D5/D6/A2/A3/A4/A5
	UNLK	A6
	RTS