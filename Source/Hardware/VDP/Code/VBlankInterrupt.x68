    ORG $0000032E
VBlankInterrupt:
    MOVEM.L   D0-D7/A0-A6,-(A7)		;Move all our registers onto the stack
    JSR       VBlank				;Perform VBlank
    MOVEM.L   (A7)+,D0-D7/A0-A6		;Move them back again
    RTE

    ORG $000005EC     
VBlank:
	MOVEM.l	    A4/A3/A2, -(A7)
	MOVEA.l	    #VBLANK_FLAG_1, A2
	MOVEA.l	    #TOEJAM_CAMERA_XPOS, A3
	MOVEA.l	    #TOEJAM_CAMERA_YPOS, A4
	TST.l	   	VBLANK_OVERRIDE		;Check this value for an address
	BEQ.b	    .StandardVBlank		;If it's 0 jump ahead to the VBlank
	MOVEA.l	    VBLANK_OVERRIDE, A0	;Otherwise move it into A0
	JSR	        (A0)				;And jump to it
	BRA.w	    EndVBlank			;Then skip the VBlank
.StandardVBlank:
	TST.b	    SKIP_VBLANK_FLAG	;Check if the Skip VBlank Flag is set
	BEQ.w	    NearEndVBlank		;If it is skip to almost the end of VBlank
	TST.b	    VBLANK_FLAG_2		;Check if this flag is set
	BEQ.b	    .CheckFlags		    ;Skip updating the camera if it is (this stops getting skipped when the main game is running)
	MOVE.w	    TOEJAM_CAMERA_NEWX, (A3)			;Copy the value from here into TOEJAM_CAMERA_XPOS
	MOVE.w	    TOEJAM_CAMERA_NEWY, (A4)			;Copy the value from here into TOEJAM_CAMERA_YPOS
	MOVE.w	    EARL_CAMERA_NEWX, EARL_CAMERA_XPOS	;Copy the value from here into EARL_CAMERA_XPOS
	MOVE.w	    EARL_CAMERA_NEWY, EARL_CAMERA_YPOS	;Copy the value from here into EARL_CAMERA_YPOS
.CheckFlags:
	TST.b	    VHBLANK_FLAG_1		;Check if this flag is clear.
	BNE.b	    .FlagsNotClear      ;Skip if it is not.
	TST.b	    (A2)                ;Check if VBLANK_FLAG_1 is clear
	BNE.b	    .FlagsNotClear      ;Skip if it is not.
	TST.b	    $1(A2)              ;Check if HBLANK_FLAG_1 is clear
	BEQ.b	    .FlagsClear         ;Skip if it is.
.FlagsNotClear:
	PEA	        $0001.w             ;Push $0001 onto the stack
	JSR	        LOC_0000CF9A        ;Call this
	ADDQ.l	    #4, A7              ;Reset the stack
	MOVE.w	    #$8A70, VDP_CONTROL_PORT    ;Instruction into the VDP
	BRA.b	    .VBlankCheck        ;Skip ahead
.FlagsClear:
	CLR.l	    -(A7)               ;Same as above but with stack cleared
	JSR	        LOC_0000CF9A
	ADDQ.l	    #4, A7
	MOVE.w	    #$8AB6, VDP_CONTROL_PORT    ;And a different instruction
.VBlankCheck:
	MOVE.w	    #$8014, VDP_CONTROL_PORT
	TST.b	    (A2)                ;Check if VBLANK_FLAG_1 is clear
	BEQ.b	    .CopyCameraToVDP    ;CopyCamera if it is.
	MOVE.w	    #$920E, VDP_CONTROL_PORT
	BRA.w	    .DoneWithCamera        ;Skip if it isn't
.CopyCameraToVDP:
	MOVE.w    	#$8228, VDP_CONTROL_PORT
	MOVE.l	    #VIDEO_SRAM, VDP_CONTROL_PORT
	MOVE.w	    (A4), VDP_DATA_PORT             ;TOEJAM_CAMERA_YPOS to VDP
	MOVE.l	    #$7C000003, VDP_CONTROL_PORT
	MOVE.w	    (A3), D0                        ;TOEJAM_CAMERA_XPOS to D0
	EXT.l	    D0
	NEG.l	    D0                              ;Negated
	MOVE.w	    D0, VDP_DATA_PORT               ;And to the VDP
	CMPI.b	    #FINAL_LEVEL, TOEJAMS_CURRENT_LEVEL ;If on the last level
	BGE.b	    .DoneWithCamera                    ;Skip ahead
	MOVE.l	    #$40020010, VDP_CONTROL_PORT
	MOVE.w	    (A4), D0                        ;TOEJAM_CAMERA_YPOS to D0
	EXT.l	    D0                              ;I think this is checking if it is negative
	BGE.b	    .JustRollY                      ;And jumping if it isn't
	NEG.l	    D0                              ;else Negate
	LSR.l	    #1, D0                          ;Roll
	NEG.l	    D0                              ;Negate
	BRA.b	    .CameraYtoVDP                   ;then continue
.JustRollY:
	LSR.l	    #1, D0                          ;Otherwise just roll
.CameraYtoVDP:
	MOVE.w    	D0, VDP_DATA_PORT
	MOVE.l    	#$7C020003, VDP_CONTROL_PORT
	MOVE.w    	(A3), D0                        ;Same as above but for TOEJAM_CAMERA_XPOS
	EXT.l	    D0
	NEG.l     	D0
	MOVE.l    	D0, D0
	BGE.b     	.JustRollX
	NEG.l     	D0
	LSR.l     	#1, D0
	NEG.l     	D0
	BRA.b	    .CameraXtoVDP    
.JustRollX:
	LSR.l	    #1, D0
.CameraXtoVDP:
	MOVE.w	    D0, VDP_DATA_PORT    
.DoneWithCamera:                            ;Sprite is after this
	CLR.l	    -(A7)
	JSR	        LOC_0000059E(PC)            ;Unknown
	TST.w	    WAIT_FOR_VBLANK_FLAG        ;Are we waiting for a VBlank?
	ADDQ.l	    #4, A7
	BEQ.b	    LOC_00000730                ;Branch if not
	JSR	        LOC_0000F67E                ;Else call this
	BRA.w	    EndReadInput                ;And jump near the end
LOC_00000730:
	TST.b	    VBLANK_FLAG_3
	BNE.w	    NearEndVBlank
	TST.b	    VBLANK_FLAG_4
	BEQ.b	    LOC_0000074C
	JSR 	    LOC_00008CE8
	BRA.w	    NearEndVBlank
LOC_0000074C:
	TST.b	    VBLANK_FLAG_2
	BEQ.b	    LOC_0000076C
	CLR.l	    -(A7)
	CLR.l	    -(A7)
	JSR	        SpriteLoadingStuff                            ;Sprite is in here!
	CLR.l	    -(A7)
	JSR	        LOC_0000059E(PC)
	LEA	        $C(A7), A7
	BRA.w	    NearEndVBlank
LOC_0000076C:
	TST.b	    VBLANK_FLAG_5
	BEQ.b	    LOC_00000790
	CLR.l	    -(A7)
	JSR			LOC_0000B3C4
	PEA 		$0001.w
	JSR			LOC_0000B3C4
	JSR	        DrawHUD
	ADDQ.l    	#8, A7
	BRA.b	    NearEndVBlank
LOC_00000790:
	PEA	        $0004.w
	JSR	        LOC_00008BD4
	ADDQ.l    	#4, A7
	TST.l     	D0
	BNE.b	    LOC_000007C4
	JSR	        DrawLevelGraphics(PC)   ;Sprite is before this
	JSR 	    UpdateCameraY.l
	JSR	        UpdateCameraX.l
	CLR.l	    -(A7)
	JSR 	    LOC_0000B16C.l
	PEA 	    $0001.w
	JSR	        LOC_0000B16C.l
	ADDQ.l	    #8, A7
LOC_000007C4:
	MOVEA.l     #LOC_00FF8024, A0
	ADDQ.b      #1, (A0)
	MOVE.b      (A0), D0
	BTST.l      #0, D0
	BEQ.b	    LOC_000007D8
	CLR.l       -(A7)
	BRA.b	    LOC_000007DC
LOC_000007D8:
	PEA	        $0001.w
LOC_000007DC:
	JSR       	LOC_0000D132
	ADDQ.l	    #4, A7
NearEndVBlank:
	TST.b	    VBLANK_FLAG_3
	BEQ.b	    LOC_000007F4
	TST.b	    VBLANK_FLAG_6
	BEQ.b	    EndReadInput
LOC_000007F4:
	MOVE.b	    VBLANK_FLAG_7, D0
	CMP.b	    VBLANK_FLAG_8, D0
LOC_00000800:
	BEQ.b	    LOC_00000822
	MOVE.b      VBLANK_FLAG_7, D0
	EXT.w	    D0
	EXT.l	    D0
	MOVE.l	    D0, -(A7)
	JSR	        LOC_0003E3FA
	ADDQ.l    	#4, A7
	MOVE.b    	VBLANK_FLAG_7, VBLANK_FLAG_8
	BRA.b     	ReadInput
LOC_00000822:
	JSR	        MassiveRoutine
ReadInput:
	BTST.b	    #0, INPUT_READING_FLAG_LO   ;Test the low byte of the reading flag
	BEQ.b	   	Pad1ReadingComplete			;If equal skip reading Pad1
	JSR	        ReadPad1Input				;Get the state of Pad1
	MOVE.w	    D0, P1_PAD_INPUT			;Move result into RAM
	BRA.b	    EndReadInput				;Skip reading Pad2
Pad1ReadingComplete:
	JSR	        ReadPad2Input				;Get the state of Pad2
	MOVE.w	    D0, P2_PAD_INPUT			;Move result into RAM
EndReadInput:
	ADDQ.w	    #1, VBLANK_COUNTER	;Increase our counter so everyone knows a VBlank has happened
EndVBlank:
	MOVEM.l	    (A7)+, A2/A3/A4
	RTS

    ORG $0000055A
SetSkipVHBlankFlags:
	MOVE.b	#1, SKIP_VBLANK_FLAG
	MOVE.b	#1, SKIP_HBLANK_FLAG
	RTS

GetSkipVBlankFlag:
	MOVE.b	SKIP_VBLANK_FLAG, D0
	EXT.w	D0
	EXT.l	D0
	RTS

WaitForNVBlanks:
	MOVE.w	$6(A7), D0						;Move the amount of VBlanks we want to loop for off the stack
	BTST.b	#6, INITIAL_STACK_POINTER		;If bit 6 of the INITIAL stack is 0
	BEQ.b	EndWaitForNVBlanks				;Don't to any waiting
	BRA.b	CheckWaitForVBlankLoop
SetupWaitForVBlankLoop:
	MOVE.w	VBLANK_COUNTER, D1				;Read this value into D1
Loop_WaitForVBlank:
	CMP.w	VBLANK_COUNTER, D1				;And loop here till this value changes
	BEQ.b	Loop_WaitForVBlank				;Once it has it means we've had a VBlank
	SUBQ.w	#1, D0							;Reduce our counter
CheckWaitForVBlankLoop:
	TST.w	D0								;If it isn't at 0 yet
	BGT.b	SetupWaitForVBlankLoop			;Loop for another VBlank
EndWaitForNVBlanks:
	RTS
	
LOC_0000059E:
	MOVE.w	$6(A7), D0
	MOVEA.l	#VBLANK_FLAG_1, A0
	TST.b	VHBLANK_FLAG_1
	BEQ.b	LOC_000005C4
	TST.w	D0
	BNE.b	LOC_000005BC
LOC_000005B4:
	JSR	    LOC_0000D002
	BRA.b	LOC_000005EA
LOC_000005BC:
	JSR	    LOC_0000D02E
	BRA.b	LOC_000005EA
LOC_000005C4:
	TST.w	D0
	BNE.b	LOC_000005CC
	TST.b	(A0)
	BNE.b	LOC_000005D8
LOC_000005CC:
	CMPI.w	#1, D0
	BNE.b	LOC_000005DA
	TST.b	$1(A0)
	BEQ.b	LOC_000005DA
LOC_000005D8:
	BRA.b	LOC_000005BC
LOC_000005DA:
	TST.w	D0
	BEQ.b	LOC_000005E2
	TST.b	(A0)
	BEQ.b	LOC_000005E4
LOC_000005E2:
	BRA.b	LOC_000005B4
LOC_000005E4:
	JSR	    LOC_0000CFEE
LOC_000005EA:
	RTS

    ORG $000019CE
UpdateCameraY:
	MOVEM.l	A3/A2/D5/D4/D3/D2, -(A7)
	MOVEA.l	#LOC_00FF8102, A2
	MOVEA.l	#LOC_00FF804E, A3
	CLR.w	D3
LOC_000019E0:
	MOVE.w	D3, D0
	EXT.l	D0
	MOVEA.l	#LOC_00FF8106, A0
	TST.b	(A0,D0.l)
	BEQ.w	LOC_00001B38
	MOVE.w	D3, D0
	EXT.l	D0
	MOVEA.l	#LOC_00FF8104, A0
	MOVE.b	(A0,D0.l), D4
	EXT.w	D4
	MOVE.w	D3, D0
	EXT.l	D0
	MOVEA.l	#LOC_00FF8108, A0
	MOVE.b	(A0,D0.l), D5
	EXT.w	D5
	TST.w	D3
	BNE.b	LOC_00001A36
	MOVE.w	D4, D0
	EXT.l	D0
	MOVEQ	#$00000011, D1
	ASL.l	D1, D0
	MOVE.w	D3, D1
	EXT.l	D1
	MOVE.b	(A2,D1.l), D1
	EXT.w	D1
	EXT.l	D1
	MOVEQ	#$00000017, D2
	ASL.l	D2, D1
	ADDI.l	#VIDEO_RAM_A000, D1
	BRA.b	LOC_00001A54
LOC_00001A36:
	MOVE.w	D4, D0
	EXT.l	D0
	MOVEQ	#$00000011, D1
	ASL.l	D1, D0
	MOVE.w	D3, D1
	EXT.l	D1
	MOVE.b	(A2,D1.l), D1
	EXT.w	D1
	EXT.l	D1
	MOVEQ	#$00000017, D2
	ASL.l	D2, D1
	ADDI.l	#VIDEO_RAM_C000, D1
LOC_00001A54:
	ADD.l	D1, D0
	MOVE.l	D0, VDP_CONTROL_PORT
	MOVE.w	D5, D0
	EXT.l	D0
	MOVE.w	D4, D1
	EXT.l	D1
	ADD.l	D1, D0
	MOVEQ	#$00000040, D1
	CMP.l	D0, D1
	BLT.b	LOC_00001A90
	MOVE.w	D5, D1
	EXT.l	D1
	MOVE.l	D1, -(A7)
	MOVE.w	D3, D1
	EXT.l	D1
	MOVE.l	D1, D0
	MULU.w	#$005A, D1
	SWAP	D0
	MULU.w	#$005A, D0
	SWAP	D0
	CLR.w	D0
	ADD.l	D0, D1
	PEA	(A3,D1.l)
	BRA.w	LOC_00001B22
LOC_00001A90:
	MOVEQ	#$00000040, D2
	SUB.w	D4, D2
	MOVE.w	D2, D1
	EXT.l	D1
	MOVE.l	D1, -(A7)
	MOVE.w	D3, D1
	EXT.l	D1
	MOVE.l	D1, D0
	MULU.w	#$005A, D1
	SWAP	D0
	MULU.w	#$005A, D0
	SWAP	D0
	CLR.w	D0
	ADD.l	D0, D1
	PEA	(A3,D1.l)
	JSR	MoveDataToVRAM
	TST.w	D3
	ADDQ.l	#8, A7
	BNE.b	LOC_00001AD8
	MOVE.w	D3, D0
	EXT.l	D0
	MOVE.b	(A2,D0.l), D0
	EXT.w	D0
	EXT.l	D0
	MOVEQ	#$00000017, D1
	ASL.l	D1, D0
	ADDI.l	#VIDEO_RAM_A000, D0
	BRA.b	LOC_00001AEE
LOC_00001AD8:
	MOVE.w	D3, D0
	EXT.l	D0
	MOVE.b	(A2,D0.l), D0
	EXT.w	D0
	EXT.l	D0
	MOVEQ	#$00000017, D1
	ASL.l	D1, D0
	ADDI.l	#VIDEO_RAM_C000, D0
LOC_00001AEE:
	MOVE.l	D0, VDP_CONTROL_PORT
	MOVE.w	D5, D1
	EXT.l	D1
	MOVE.w	D2, D0
	EXT.l	D0
	SUB.l	D0, D1
	MOVE.l	D1, -(A7)
	MOVE.w	D3, D1
	EXT.l	D1
	MOVE.l	D1, D0
	MULU.w	#$005A, D1
	SWAP	D0
	MULU.w	#$005A, D0
	SWAP	D0
	CLR.w	D0
	ADD.l	D0, D1
	MOVEA.l	A3, A0
	MOVE.w	D2, D0
	ADD.w	D0, D0
	ADDA.l	D1, A0
	PEA	(A0,D0.w)
LOC_00001B22:
	JSR	MoveDataToVRAM
	ADDQ.l	#8, A7
	MOVE.w	D3, D0
	EXT.l	D0
	MOVEA.l	#LOC_00FF8106, A0
	CLR.b	(A0,D0.l)
LOC_00001B38:
	ADDQ.w	#1, D3
	CMPI.w	#2, D3
	BLT.w	LOC_000019E0
	MOVEM.l	(A7)+, D2/D3/D4/D5/A2/A3
	RTS

    ORG $00001EAC
UpdateCameraX:
	MOVEM.l	A3/A2/D5/D4/D3/D2, -(A7)
	MOVEA.l	#LOC_00FF818C, A2
	MOVEA.l	#LOC_00FF810A, A3
	MOVE.w	#$8F80, VDP_CONTROL_PORT
	CLR.w	D3
LOC_00001EC6:
	MOVE.w	D3, D0
	EXT.l	D0
	MOVEA.l	#LOC_00FF818E, A0
	TST.b	(A0,D0.l)
	BEQ.w	LOC_00001FF2
	MOVE.w	D3, D0
	EXT.l	D0
	MOVEA.l	#LOC_00FF818A, A0
	MOVE.b	(A0,D0.l), D5
	EXT.w	D5
	MOVE.w	D3, D0
	EXT.l	D0
	MOVEA.l	#LOC_00FF8190, A0
	MOVE.b	(A0,D0.l), D4
	TST.w	D3
	BNE.b	LOC_00001F1A
	MOVE.w	D3, D0
	EXT.l	D0
	MOVE.b	(A2,D0.l), D0
	EXT.w	D0
	EXT.l	D0
	MOVEQ	#$00000011, D1
	ASL.l	D1, D0
	MOVE.w	D5, D2
	EXT.l	D2
	MOVEQ	#$00000017, D1
	ASL.l	D1, D2
	ADDI.l	#VIDEO_RAM_A000, D2
	BRA.b	LOC_00001F38
LOC_00001F1A:
	MOVE.w	D3, D0
	EXT.l	D0
	MOVE.b	(A2,D0.l), D0
	EXT.w	D0
	EXT.l	D0
	MOVEQ	#$00000011, D1
	ASL.l	D1, D0
	MOVE.w	D5, D2
	EXT.l	D2
	MOVEQ	#$00000017, D1
	ASL.l	D1, D2
	ADDI.l	#VIDEO_RAM_C000, D2
LOC_00001F38:
	ADD.l	D2, D0
	MOVE.l	D0, VDP_CONTROL_PORT
	MOVE.b	D4, D0
	EXT.w	D0
	EXT.l	D0
	MOVE.w	D5, D1
	EXT.l	D1
	ADD.l	D1, D0
	MOVEQ	#$00000020, D1
	CMP.l	D0, D1
	BLT.b	LOC_00001F68
	MOVE.b	D4, D1
	EXT.w	D1
	EXT.l	D1
	MOVE.l	D1, -(A7)
	MOVE.w	D3, D0
	EXT.l	D0
	ASL.l	#6, D0
	PEA	(A3,D0.l)
	BRA.w	LOC_00001FDC
LOC_00001F68:
	MOVEQ	#$00000020, D2
	SUB.w	D5, D2
	MOVE.w	D2, D1
	EXT.l	D1
	MOVE.l	D1, -(A7)
	MOVE.w	D3, D0
	EXT.l	D0
	ASL.l	#6, D0
	PEA	(A3,D0.l)
	JSR	MoveDataToVRAM
	TST.w	D3
	ADDQ.l	#8, A7
	BNE.b	LOC_00001FA0
	MOVE.w	D3, D0
	EXT.l	D0
	MOVE.b	(A2,D0.l), D0
	EXT.w	D0
	EXT.l	D0
	MOVEQ	#$00000011, D1
	ASL.l	D1, D0
	ADDI.l	#VIDEO_RAM_A000, D0
	BRA.b	LOC_00001FB6
LOC_00001FA0:
	MOVE.w	D3, D0
	EXT.l	D0
	MOVE.b	(A2,D0.l), D0
	EXT.w	D0
	EXT.l	D0
	MOVEQ	#$00000011, D1
	ASL.l	D1, D0
	ADDI.l	#VIDEO_RAM_C000, D0
LOC_00001FB6:
	MOVE.l	D0, VDP_CONTROL_PORT
	MOVE.b	D4, D1
	EXT.w	D1
	EXT.l	D1
	MOVE.w	D2, D0
	EXT.l	D0
	SUB.l	D0, D1
	MOVE.l	D1, -(A7)
	MOVE.w	D3, D0
	EXT.l	D0
	ASL.l	#6, D0
	MOVEA.l	A3, A0
	MOVE.w	D2, D1
	ADD.w	D1, D1
	ADDA.l	D0, A0
	PEA	(A0,D1.w)
LOC_00001FDC:
	JSR	MoveDataToVRAM
	ADDQ.l	#8, A7
	MOVE.w	D3, D0
	EXT.l	D0
	MOVEA.l	#LOC_00FF818E, A0
	CLR.b	(A0,D0.l)
LOC_00001FF2:
	ADDQ.w	#1, D3
	CMPI.w	#2, D3
	BLT.w	LOC_00001EC6
	MOVE.w	#$8F02, VDP_CONTROL_PORT
	MOVEM.l	(A7)+, D2/D3/D4/D5/A2/A3
	RTS

    ORG $0000D132
LOC_0000D132:
	MOVE.l	D2, -(A7)
	MOVE.w	$A(A7), D0
	ASL.w	#7, D0
	MOVEA.l	#TOEJAMS_XPOS, A0
	ADDA.w	D0, A0
	MOVEA.l	A0, A1
	BTST.b	#4, $4D(A1)
	BEQ.b	LOC_0000D152
	TST.b	$4A(A1)
	BNE.b	LOC_0000D154
LOC_0000D152:
	BRA.b	LOC_0000D1A0
LOC_0000D154:
	BTST.b	#1, $2F(A1)         ;Check LOC_00FFA289
	BEQ.b	LOC_0000D162
	LEA	$30(A1), A0
	BRA.b	LOC_0000D170
LOC_0000D162:
	BTST.b	#2, $2F(A1)         ;Check LOC_00FFA289
	BNE.b	LOC_0000D16C
	BRA.b	LOC_0000D1A0
LOC_0000D16C:
	LEA	$3C(A1), A0
LOC_0000D170:
	MOVEQ	#0, D0
	MOVE.b	$2E(A1), D0
	MOVE.w	D0, D2
	BRA.b	LOC_0000D19C
LOC_0000D17A:
	MOVE.b	(A0)+, D0
	MOVE.b	D0, D0
	EXT.w	D0
	EXT.l	D0
	MOVEQ	#$00000013, D1
	ASL.l	D1, D0
	ADDI.l	#$78000003, D0
	MOVE.l	D0, VDP_CONTROL_PORT
	MOVE.w	#1, VDP_DATA_PORT
	SUBQ.w	#1, D2
LOC_0000D19C:
	TST.w	D2
	BGT.b	LOC_0000D17A
LOC_0000D1A0:
	MOVE.l	(A7)+, D2
	RTS

    ORG $0000F67E
LOC_0000F67E:
	LINK	A6, #-($0C)
	MOVEM.l	D6/D5/D4/D3/D2, -(A7)
	MOVE.w	LOC_00FFDA32, D0
	ASL.w	#7, D0
	MOVEA.l	#TOEJAMS_XPOS, A0
	ADDA.w	D0, A0
	MOVE.w	(A0), D0
	MOVE.w	$2(A0), D1
	MOVE.w	D1, -$6(A6)
	MOVE.w	-$6(A6), D2
	ASR.w	#3, D2
	MOVE.w	D2, -$6(A6)
	MOVE.w	-$6(A6), -$C(A6)
	SUBQ.w	#6, -$C(A6)
	MOVE.w	-$6(A6), -$8(A6)
	ADDQ.w	#6, -$8(A6)
	ASR.w	#3, D0
	SUBQ.w	#4, D0
	MOVE.w	D0, -$A(A6)
	ADDI.w	#9, -$A(A6)
	CLR.w	-$2(A6)
	CMPI.w	#1, LOC_00FFDA32
	BNE.b	LOC_0000F6E8
	TST.b	VHBLANK_FLAG_1
	BEQ.b	LOC_0000F6E8
	MOVEQ	#1, D1
	MOVE.w	D1, -$2(A6)
LOC_0000F6E8:
	MOVE.w	#$8C81, VDP_CONTROL_PORT
	MOVE.w	D0, -$4(A6)
	BRA.w	LOC_0000F7C0
LOC_0000F6F8:
	MOVE.w	-$C(A6), D6
	BRA.w	LOC_0000F7B4
LOC_0000F700:
	MOVE.w	D6, D3
	ANDI.w	#$001F, D3
	MOVE.w	-$4(A6), D4
	ANDI.w	#$003F, D4
	TST.w	-$2(A6)
	BNE.b	LOC_0000F72C
	MOVE.w	D4, D0
	EXT.l	D0
	MOVEQ	#$00000011, D1
	ASL.l	D1, D0
	MOVE.w	D3, D1
	EXT.l	D1
	MOVEQ	#$00000017, D2
	ASL.l	D2, D1
	ADDI.l	#$20000002, D1
	BRA.b	LOC_0000F73E
LOC_0000F72C:
	MOVE.w	D4, D0
	EXT.l	D0
	MOVEQ	#$00000011, D1
	ASL.l	D1, D0
	MOVE.w	D3, D1
	EXT.l	D1
	MOVEQ	#$00000017, D2
	ASL.l	D2, D1
	ADDQ.l	#3, D1
LOC_0000F73E:
	ADD.l	D1, D0
	MOVE.l	D0, VDP_CONTROL_PORT
	MOVE.w	VDP_DATA_PORT, D5
	CMP.w	-$6(A6), D6
	BLE.b	LOC_0000F76C
	MOVEQ	#0, D0
	MOVE.w	D5, D0
	ANDI.l	#$000007FF, D0
	MOVEA.l	#LOC_000ADF8E, A0
	MOVE.b	(A0,D0.l), D0
	CMPI.b	#$90, D0
	BCS.b	LOC_0000F7BC
LOC_0000F76C:
	ANDI.w	#$7FFF, D5
	TST.w	-$2(A6)
	BNE.b	LOC_0000F78E
	MOVE.w	D4, D0
	EXT.l	D0
	MOVEQ	#$00000011, D1
	ASL.l	D1, D0
	MOVE.w	D3, D1
	EXT.l	D1
	MOVEQ	#$00000017, D2
	ASL.l	D2, D1
	ADDI.l	#VIDEO_RAM_A000, D1
	BRA.b	LOC_0000F7A4
LOC_0000F78E:
	MOVE.w	D4, D0
	EXT.l	D0
	MOVEQ	#$00000011, D1
	ASL.l	D1, D0
	MOVE.w	D3, D1
	EXT.l	D1
	MOVEQ	#$00000017, D2
	ASL.l	D2, D1
	ADDI.l	#VIDEO_RAM_C000, D1
LOC_0000F7A4:
	ADD.l	D1, D0
	MOVE.l	D0, VDP_CONTROL_PORT
	MOVE.w	D5, VDP_DATA_PORT
	ADDQ.w	#1, D6
LOC_0000F7B4:
	CMP.w	-$8(A6), D6
	BLE.w	LOC_0000F700
LOC_0000F7BC:
	ADDQ.w	#1, -$4(A6)
LOC_0000F7C0:
	MOVE.w	-$4(A6), D0
	CMP.w	-$A(A6), D0
	BLE.w	LOC_0000F6F8
	CLR.w	WAIT_FOR_VBLANK_FLAG
	MOVEM.l	-$20(A6), D2/D3/D4/D5/D6
	UNLK	A6
	RTS

    ORG $0003E3D6
LOC_0003E3D6:
	MOVEM.l	A5/D3, -(A7)
	MOVEQ	#5, D1
LOC_0003E3DC:
	MOVEM.l	D1, -(A7)
	JSR	LOC_0003EC04(PC)
	NOP
	MOVEM.l	(A7)+, D1
	DBF	D1, LOC_0003E3DC
	MOVEM.l	(A7)+, D3/A5
	CLR.w	LOC_00FFE478
	RTS

    ORG $0003E3FA
LOC_0003E3FA:
	MOVE.w	#2, LOC_00FFE486
	TST.w	LOC_00FFE476
	BEQ.b	LOC_0003E40E
	ADDQ.w	#7, $6(A7)
LOC_0003E40E:
	JSR	    LOC_0003E3D6(PC)
	JSR	    RAMWritesRoutine1(PC)
	MOVE.l	$4(A7), D0
	MULU.w	#$00E4, D0
	LEA	    RAMWritesRoutine1ValuesB(PC), A0
	ADDA.l	D0, A0
	LEA	    LOC_00FFFE30, A1
	MOVE.l	#$000000E4, D0
	SUBQ.w	#1, D0
LOC_0003E432:
	MOVE.b	(A0)+, (A1)+
	DBF	    D0, LOC_0003E432
	RTS

	CLR.l	$8(A0)
	MOVEQ	#0, D0
	MOVE.b	VBLANK_FLAG_8, D0
	SUBQ.w	#1, LOC_00FFE486
	BEQ.b	LOC_0003E46C
	MOVE.w	LOC_00FFE486, D1
	CMPI.w	#$FFFE, D1
	BEQ.b	LOC_0003E45C
	RTS

LOC_0003E45C:
	TST.w	LOC_00FFE476
	BNE.b	LOC_0003E46C
	MOVE.l	D0, -(A7)
	BSR.b	LOC_0003E3FA
	ADDQ.w	#4, A7
	RTS	

    ORG $0003E46C
LOC_0003E46C:
	TST.w	LOC_00FFE474
	BNE.b	LOC_0003E47E
	ADDQ.b	#7, D0
	MOVE.l	D0, -(A7)
	BSR.b	LOC_0003E40E
	ADDQ.l	#4, A7
	RTS

    ORG $0003E47E
LOC_0003E47E:
	MOVE.w	#1, LOC_00FFE486
	RTS